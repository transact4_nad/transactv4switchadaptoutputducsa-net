﻿Imports TransActV4.Switch.Interface.INegocio
Imports TransActV4.Switch.Interface.IAdaptOutput
Imports TransActV4.Switch.Entidad.Transacciones
Public Class AdaptOutput_IN
    Implements IAdaptOutput_IN

    Public Const DECRETO_NO_ESPECIFICADO As String = "99999"
    Private Const NOMBRE_CONCENTRADOR As String = "DUCSA"
    Private Const CARPETA_TRAZA As String = "Traza\"
    Private Const CODRESPADQ_INICIEBATCH As String = "95"
    Private Const CODRESPADQ_ERRORSISTEMA As String = "96"
    Private Const CODRESPADQ_REALIZAROFFLINE1 As String = "01"
    Private Const CODRESPADQ_REALIZAROFFLINE2 As String = "02"
    Private Const CODRESPADQ_EMISORNORESPONDE As String = "91"
    Private Const TIPOTARJETA_DEBITO As String = "DEB"

    Private _objNegocio As INegocio
    Private _TrazaEncendida As Boolean

    Public Sub Inicializar(objNeg As INegocio, TrazaEncendida As Boolean) Implements IAdaptOutput_IN.Inicializar
        _objNegocio = objNeg
        _TrazaEncendida = TrazaEncendida
    End Sub

    Public Function ObtenerTransaccion(strMensaje As String) As TransaccionRespuesta Implements IAdaptOutput_IN.ObtenerTransaccion
        'GrabarDebug(strMensaje)
        Dim objTrnResp As TransaccionRespuesta
        Dim Mensaje As absMensajeRecibidoDelProcesador
        'LARGO(4) + ADQID(2)
        strMensaje = Mid(strMensaje, 7)
        Mensaje = absMensajeRecibidoDelProcesador.ConstruirMensajeRecibido(strMensaje)
        objTrnResp = Mensaje.ObtenerTransaccion()
        GrabarDebug(strMensaje, objTrnResp)
        Return objTrnResp
    End Function

    Public Sub ComplementarTransaccionRespuesta(objTrn As Transaccion, objTrnResp As TransaccionRespuesta) Implements IAdaptOutput_IN.ComplementarTransaccionRespuesta
        If objTrnResp.TipoTrnResp = TransaccionRespuesta.TiposTransaccionRespuesta.TIPOTRN_RESPUESTA_TARJETA_CIERRE Then
            Dim objTrnTarjResp As TrnRespTarjCierre
            Dim objTrnTarj As TrnTarjCierre
            objTrnTarjResp = DirectCast(objTrnResp, TrnRespTarjCierre)
            objTrnTarj = DirectCast(objTrn, TrnTarjCierre)
            If objTrnTarjResp.CodRespAdq = CODRESPADQ_INICIEBATCH AndAlso objTrnTarj.EsBatchUpload Then
                objTrnTarjResp.CodRespAdq = CODRESPADQ_ERRORSISTEMA
            End If
        ElseIf objTrnResp.TipoTrnResp = TransaccionRespuesta.TiposTransaccionRespuesta.TIPOTRN_RESPUESTA_TARJETA_TRANSACCION Then
            Dim objTrnTarjResp As TrnRespTarjTransaccion
            Dim objTrnTarj As TrnTarjTransaccion
            objTrnTarjResp = DirectCast(objTrnResp, TrnRespTarjTransaccion)
            objTrnTarj = DirectCast(objTrn, TrnTarjTransaccion)

            If (objTrnTarjResp.CodRespAdq = CODRESPADQ_REALIZAROFFLINE1 Or objTrnTarjResp.CodRespAdq = CODRESPADQ_REALIZAROFFLINE2) AndAlso objTrnTarj.Tarjeta.TipoTarjeta = TIPOTARJETA_DEBITO Then
                objTrnTarjResp.CodRespAdq = CODRESPADQ_EMISORNORESPONDE
            End If

            If objTrnTarjResp.DecretoLeyNro = DECRETO_NO_ESPECIFICADO Then
                If Trim(objTrnTarj.DecretoLey.DecretoLeyAdqId) <> "0" Then
                    objTrnTarjResp.DecretoLeyAplicado = True
                End If
                objTrnTarjResp.DecretoLeyNro = objTrnTarj.DecretoLey.DecretoLeySwNro
                objTrnTarjResp.DecretoLeyMonto = 0
            End If

            If objTrnTarj.Offline.EsOffline AndAlso Trim(objTrnTarjResp.NroAutorizacion).Replace("0", "") = "" Then
                objTrnTarjResp.NroAutorizacion = objTrnTarj.Offline.NroAutorizacionOffline
                objTrnTarjResp.ChipEmv.AuthCod = objTrnTarj.Offline.NroAutorizacionOffline
            End If
        ElseIf objTrnResp.TipoTrnResp = TransaccionRespuesta.TiposTransaccionRespuesta.TIPOTRN_RESPUESTA_CONSULTA_SALDO Then
            Dim objTrnTarjResp As TrnRespTrnTarjConsultaSaldo
            Dim objTrnTarj As TrnTarjConsultaSaldo
            objTrnTarjResp = DirectCast(objTrnResp, TrnRespTrnTarjConsultaSaldo)
            objTrnTarj = DirectCast(objTrn, TrnTarjConsultaSaldo)

            If (objTrnTarjResp.CodRespAdq = CODRESPADQ_REALIZAROFFLINE1 Or objTrnTarjResp.CodRespAdq = CODRESPADQ_REALIZAROFFLINE2) Then
                objTrnTarjResp.CodRespAdq = CODRESPADQ_EMISORNORESPONDE
            End If
        ElseIf objTrnResp.TipoTrnResp = TransaccionRespuesta.TiposTransaccionRespuesta.TIPOTRN_RESPUESTA_TARJETA_TRANSACCION_CONFIRMA_PROCESADOR Then
            Dim objTrnTarjResp As TrnRespTarjTransaccionConfirmaProcesador
            Dim objTrnTarj As TrnTarjTransaccionConfirmaProcesador
            objTrnTarjResp = DirectCast(objTrnResp, TrnRespTarjTransaccionConfirmaProcesador)
            objTrnTarj = DirectCast(objTrn, TrnTarjTransaccionConfirmaProcesador)

            If (objTrnTarjResp.CodRespAdq = CODRESPADQ_REALIZAROFFLINE1 Or objTrnTarjResp.CodRespAdq = CODRESPADQ_REALIZAROFFLINE2) Then
                objTrnTarjResp.CodRespAdq = CODRESPADQ_EMISORNORESPONDE
            End If
        End If
    End Sub

#Region "Debug"
    Private Sub GrabarDebug(strMensaje As String)
        If Not _TrazaEncendida Then Exit Sub
        Dim SW As System.IO.StreamWriter = Nothing
        Dim FtoASCII As New TransActV4.Common.Formatter.FormatoASCII
        Try
            If Not IO.Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory & CARPETA_TRAZA) Then
                IO.Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory & CARPETA_TRAZA)
            End If
            Dim RutaArchivo As String = System.AppDomain.CurrentDomain.BaseDirectory & CARPETA_TRAZA & NOMBRE_CONCENTRADOR & "_" & Now.Ticks & ".txt"
            SW = New System.IO.StreamWriter(RutaArchivo)
            SW.Write(FtoASCII.ASCIIToHEX(strMensaje))
        Catch ex As Exception
        Finally
            If Not IsNothing(SW) Then
                SW.Close()
            End If
        End Try
    End Sub

    Private Sub GrabarDebug(strMensaje As String, objTrn As TransaccionRespuesta)
        If Not _TrazaEncendida Then Exit Sub
        If objTrn.TipoTrnResp = TransaccionRespuesta.TiposTransaccionRespuesta.TIPOTRN_RESPUESTA_TARJETA_ECHOTEST Then Exit Sub
        Dim SW As System.IO.StreamWriter = Nothing
        Dim FtoASCII As New TransActV4.Common.Formatter.FormatoASCII
        Try
            If Not IO.Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory & CARPETA_TRAZA) Then
                IO.Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory & CARPETA_TRAZA)
            End If
            Dim RutaArchivo As String = System.AppDomain.CurrentDomain.BaseDirectory & CARPETA_TRAZA & NOMBRE_CONCENTRADOR & "_" & Now.Ticks & ".txt"
            SW = New System.IO.StreamWriter(RutaArchivo)
            SW.Write(FtoASCII.ASCIIToHEX(strMensaje))
        Catch ex As Exception
        Finally
            If Not IsNothing(SW) Then
                SW.Close()
            End If
        End Try
    End Sub
#End Region

End Class
