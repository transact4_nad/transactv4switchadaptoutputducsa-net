﻿Imports System.Runtime.Serialization
Imports TransActV4.Switch.Interface.IAdaptOutput
Imports TransActV4.Switch.AdaptOutput.Ducsa.DiccionarioMensajes

<Serializable()> Public Class ExceptionOutputAdapterTipoNoSoportado
    Inherits ExceptionOutputAdapter

    Private _Msg As String = MSG_TIPO_NO_SOPORTADO_POR_OUTPUTADAPTER
    Private _Valores As String

    Public Sub New(Valores As String)
        _Valores = Valores
    End Sub

    Public Overrides ReadOnly Property Message As String
        Get
            Return _Msg
        End Get
    End Property

    Public Overrides ReadOnly Property TipoException As TiposException
        Get
            Return TiposException.ExError
        End Get
    End Property

    Public Overrides ReadOnly Property MensajePrivado As String
        Get
            Return _Msg
        End Get
    End Property

    Public Overrides ReadOnly Property MensajeSolucion As String
        Get
            Return _Msg
        End Get
    End Property

    Public Overrides ReadOnly Property TipoNotificacion As TiposNotificacion
        Get
            Return TiposNotificacion.Diario
        End Get
    End Property
End Class
