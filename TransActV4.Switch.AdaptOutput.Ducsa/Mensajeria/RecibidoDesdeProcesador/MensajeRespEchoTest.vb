﻿Imports TransActV4.Common.ISO8583
Imports TransActV4.Switch.Entidad.Transacciones

Friend Class MensajeRespEchoTest
    Inherits absMensajeRecibidoDelProcesador

    Private _objISO As ISO8583Mensaje

    Public Sub New(objISO As ISO8583Mensaje)
        _objISO = objISO
    End Sub

    Public Overrides Function ObtenerTransaccion() As TransaccionRespuesta
        Dim objTrnResp As TrnRespTarjEchoTest = TransActV4.Switch.Entidad.Transacciones.Factory.GetInstance.ConstruirTrnRespTarjEchoTest

        objTrnResp.Aprobada = False
        objTrnResp.Trace = Val(_objISO.ValorCampo("011"))
        objTrnResp.CodRespAdq = _objISO.ValorCampo("039")
        objTrnResp.TID = _objISO.ValorCampo("041")
        objTrnResp.TextoAdicionalPantalla = _objISO.ValorCampo("063")

        Return objTrnResp
    End Function
End Class
