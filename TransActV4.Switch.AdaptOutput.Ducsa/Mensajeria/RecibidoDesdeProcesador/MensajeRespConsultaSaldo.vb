﻿Imports TransActV4.Common.ISO8583
Imports TransActV4.Switch.Entidad.Transacciones
Friend Class MensajeRespConsultaSaldo
    Inherits absMensajeRecibidoDelProcesador

    Private _objISO As ISO8583Mensaje

    Public Sub New(objISO As ISO8583Mensaje)
        _objISO = objISO
    End Sub

    Public Overrides Function ObtenerTransaccion() As TransaccionRespuesta
        Dim objTrnResp As TrnRespTrnTarjConsultaSaldo = TransActV4.Switch.Entidad.Transacciones.Factory.GetInstance.ConstruirTrnRespTrnTarjConsultaSaldo
        Dim FtoASCII As New TransActV4.Common.Formatter.FormatoASCII
        Dim Campo, Texto As String

        objTrnResp.Aprobada = False
        objTrnResp.Trace = Val(_objISO.ValorCampo("011"))
        objTrnResp.RRN = _objISO.ValorCampo("037")
        objTrnResp.NroAutorizacion = _objISO.ValorCampo("038")
        objTrnResp.CodRespAdq = _objISO.ValorCampo("039")
        objTrnResp.TID = _objISO.ValorCampo("041")

        objTrnResp.FirmarVoucher = False
        Campo = _objISO.ValorCampo("063")
        If Trim(Campo) <> "" Then
            If EsCampoISO63Texto(Campo) Then
                objTrnResp.TextoAdicionalVoucher = Mid(Campo, 1, 100)
            Else
                objTrnResp.TextoAdicionalPantalla = ObtenerSubCampoISO63(Campo, "0022")
                objTrnResp.TextoAdicionalVoucher = ObtenerSubCampoISO63(Campo, "0029")
                objTrnResp.FirmarVoucher = (ObtenerSubCampoISO63(Campo, "0032") <> "0")  'Si esta "0"
                Texto = Trim(ObtenerSubCampoISO63(Campo, "0033"))
                If Texto <> "" Then
                    objTrnResp.TextoAdicionalVoucher = Texto
                End If
            End If
        End If

        Return objTrnResp
    End Function


End Class
