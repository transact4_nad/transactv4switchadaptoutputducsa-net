﻿Imports TransActV4.Common.ISO8583
Imports TransActV4.Switch.Entidad.Transacciones
Friend MustInherit Class absMensajeRecibidoDelProcesador

#Region "Declaraciones"
    Private Const MENSAJE_CERRADO As Boolean = True
    Private Const LEN_HEAD As Integer = 4

    Private Const TIPO_MENSAJE_COMPRA As String = "0210"
    Private Const TIPO_MENSAJE_COMPRA_REVERSO As String = "0410"
    Private Const TIPO_MENSAJE_CIERRE As String = "0510"
    Private Const TIPO_MENSAJE_SALDO As String = "0910"
    Private Const TIPO_MENSAJE_ECHO As String = "0810"
    Private Const TIPO_MENSAJE_CONFIRMACION As String = "0710"

    Public Const CAMPO_DISNROSERIE As String = "0001"
    Public Const CAMPO_DOCIDENTIDAD As String = "0002"
    Public Const CAMPO_CONVENIO_RUT As String = "0003"
    Public Const CAMPO_CONVENIO_IMPORTE As String = "0004"
    Public Const CAMPO_CONVENIO_PRECIO As String = "0005"
    Public Const CAMPO_XMLPRODUCTOS As String = "0006"
    Public Const CAMPO_CONVENIO_KILOMETRAJE As String = "0008"
    Public Const CAMPO_CONVENIO_NOMBRE As String = "0009"
    Public Const CAMPO_CONVENIO_TEXTO_LEGAL As String = "0010"
    Public Const CAMPO_CONVENIO_NUMERO As String = "0011"
    Public Const CAMPO_CONVENIO_EMPRESA As String = "0012"
    Public Const CAMPO_CONVENIO_NOMBRE_CLIENTE As String = "0013"
    Public Const CAMPO_CONVENIO_MATRICULA As String = "0014"
    Public Const CAMPO_XMLPRODUCTOS_RESP As String = "0015"
    Public Const CAMPO_CONVENIO_AUTORIZACION As String = "0016"

#End Region

#Region "Abstract Factory de sus Hijos"

    Shared Function ConstruirMensajeRecibido(strMensajeRecibido As String) As absMensajeRecibidoDelProcesador
        Dim objISO As New ISO8583Mensaje(MENSAJE_CERRADO)
        objISO.LenHead = LEN_HEAD
        If Not objISO.ToClaseISO(strMensajeRecibido) Then
            Throw New Exception("ERROR MENSAJE ISO OUTPUTADAPTER DUCSA")
        End If

        Select Case objISO.TipoMensaje
            Case TIPO_MENSAJE_COMPRA
                Return New MensajeRespTransaccion(objISO)
            Case TIPO_MENSAJE_COMPRA_REVERSO
                Return New MensajeRespReversoTransaccion(objISO)
            Case TIPO_MENSAJE_CIERRE
                Return New MensajeRespCierre(objISO)
            Case TIPO_MENSAJE_ECHO
                Return New MensajeRespEchoTest(objISO)
            Case TIPO_MENSAJE_SALDO
                Return New MensajeRespConsultaSaldo(objISO)
            Case TIPO_MENSAJE_CONFIRMACION
                Return New MensajeRespTrnTarjTransaccionConfirmaProcesador(objISO)
            Case Else
                Throw New ExceptionOutputAdapterTipoNoSoportado("TIPO MENSAJE ISO NO SOPORTADO POR OUTPUTADAPTER DUCSA, TIPO : " & objISO.TipoMensaje)
        End Select
    End Function

#End Region



#Region "Abstraccion"

    MustOverride Function ObtenerTransaccion() As TransaccionRespuesta


#End Region

#Region "Metodos de sus Hijos"
    Friend Function ObtenerSubCampoISO63(ByVal Texto As String, ByVal SubCampo As String) As String
        Dim FtoASCII As New TransActV4.Common.Formatter.FormatoASCII
        Dim TxtCampo, Campo, Valor As String, Largo, Iteracion As Integer

        Valor = ""
        Try
            Iteracion = 0
            TxtCampo = Texto
            Do While True
                Largo = Val(FtoASCII.BCDtoASCII(Mid(TxtCampo, 1, 2)))
                If Largo < 1 Then
                    Exit Do
                End If
                'SubCampo (3)
                Campo = FtoASCII.BCDtoASCII(Mid(TxtCampo, 3, 2))
                If Campo = SubCampo Then
                    Select Case SubCampo
                        Case "0030", "0031", "0032"
                            Valor = "0"
                        Case Else
                            Valor = Mid(TxtCampo, 5, Largo - 2)
                    End Select
                    Exit Do
                End If
                TxtCampo = Mid(TxtCampo, Largo + 2 + 1)
                Iteracion += 1
                If Trim(TxtCampo) = "" OrElse Iteracion > 25 Then
                    Exit Do
                End If
            Loop
        Catch ex As Exception
        End Try
        Return Valor
    End Function

    Friend Function EsCampoISO63Texto(ByVal Texto As String) As Boolean
        Const sExpAlf = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ.,:- "
        Dim nPos As Integer, nCantCar As Integer

        EsCampoISO63Texto = True
        Texto = Texto.ToUpper()
        If Trim(Texto) <> "" Then
            EsCampoISO63Texto = False
            nPos = 0
            For nPos = 1 To 4
                If InStr(1, sExpAlf, Mid(Texto, nPos, 1)) > 0 Then
                    nCantCar = nCantCar + 1
                End If
            Next
            If nCantCar > 2 Then
                EsCampoISO63Texto = True
            End If
        End If

    End Function

    Friend Function ObtenerValorTLV(tlvTag As String, Tag As String) As String
        Dim tlvLen As Integer = 0
        Dim tlvValue As String = ""
        Dim tagRestante As String = ""

        If Tag.Length > 7 Then
            Dim tagInicial = Tag.Substring(0, 4)
            If tagInicial.Equals(tlvTag) Then
                tlvLen = Val(Tag.Substring(4, 3))
                tlvValue = Tag.Substring(7, tlvLen)
            Else
                tlvLen = Val(Tag.Substring(4, 3))
                tagRestante = Tag.Substring(7 + tlvLen)
                Return ObtenerValorTLV(tlvTag, tagRestante)
            End If
        End If
        Return tlvValue

    End Function
#End Region


End Class
