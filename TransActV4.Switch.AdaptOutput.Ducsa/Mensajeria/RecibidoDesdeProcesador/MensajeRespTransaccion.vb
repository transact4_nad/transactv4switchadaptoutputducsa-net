﻿Imports TransActV4.Common.ISO8583
Imports TransActV4.Switch.Entidad.Transacciones

Friend Class MensajeRespTransaccion
    Inherits absMensajeRecibidoDelProcesador


    Private _objISO As ISO8583Mensaje

    Public Sub New(objISO As ISO8583Mensaje)
        _objISO = objISO
    End Sub

    Public Overrides Function ObtenerTransaccion() As TransaccionRespuesta
        Dim objTrnResp As TrnRespTarjTransaccion = TransActV4.Switch.Entidad.Transacciones.Factory.GetInstance.ConstruirTrnRespTarjTransaccion
        Dim FtoASCII As New TransActV4.Common.Formatter.FormatoASCII
        Dim Campo, Texto As String

        objTrnResp.Aprobada = False
        objTrnResp.Trace = Val(_objISO.ValorCampo("011"))
        objTrnResp.RRN = _objISO.ValorCampo("037")
        objTrnResp.NroAutorizacion = _objISO.ValorCampo("038")
        objTrnResp.CodRespAdq = _objISO.ValorCampo("039")
        objTrnResp.TID = _objISO.ValorCampo("041")

        objTrnResp.EsExtranjera = False
        objTrnResp.DecretoLeyAplicado = False
        objTrnResp.DecretoLeyNro = AdaptOutput_IN.DECRETO_NO_ESPECIFICADO
        objTrnResp.DecretoLeyMonto = 0

        Dim datosAdicionales As String = _objISO.ValorCampo("059")

        If datosAdicionales.Length > 0 Then
            objTrnResp.Fidelidad.ConvenioRut = (Val(ObtenerValorTLV(CAMPO_CONVENIO_RUT, datosAdicionales)))
            objTrnResp.Fidelidad.ConvenioImporte = (Val(ObtenerValorTLV(CAMPO_CONVENIO_IMPORTE, datosAdicionales)))
            objTrnResp.Fidelidad.ConvenioPrecio = (Val(ObtenerValorTLV(CAMPO_CONVENIO_PRECIO, datosAdicionales)))
            objTrnResp.Fidelidad.ConvenioNombre = ObtenerValorTLV(CAMPO_CONVENIO_NOMBRE, datosAdicionales)
            objTrnResp.Fidelidad.ConvenioNumero = ObtenerValorTLV(CAMPO_CONVENIO_NUMERO, datosAdicionales)
            objTrnResp.Fidelidad.ConvenioEmpresa = ObtenerValorTLV(CAMPO_CONVENIO_EMPRESA, datosAdicionales)
            objTrnResp.Fidelidad.ConvenioMatricula = ObtenerValorTLV(CAMPO_CONVENIO_MATRICULA, datosAdicionales)
            objTrnResp.Fidelidad.ProductosResp = ObtenerValorTLV(CAMPO_XMLPRODUCTOS_RESP, datosAdicionales)
            objTrnResp.Fidelidad.ConvenioTextoLegal = ObtenerValorTLV(CAMPO_CONVENIO_TEXTO_LEGAL, datosAdicionales)
            objTrnResp.Fidelidad.Nombre = ObtenerValorTLV(CAMPO_CONVENIO_NOMBRE_CLIENTE, datosAdicionales)
            objTrnResp.Fidelidad.ConvenioAutorizacion = ObtenerValorTLV(CAMPO_CONVENIO_AUTORIZACION, datosAdicionales)
        End If

        objTrnResp.FirmarVoucher = True
        Campo = _objISO.ValorCampo("063")
        If Trim(Campo) <> "" Then
            If EsCampoISO63Texto(Campo) Then
                objTrnResp.TextoAdicionalVoucher = Mid(Campo, 1, 100)
            Else
                objTrnResp.TextoAdicionalPantalla = ObtenerSubCampoISO63(Campo, "0022")
                objTrnResp.TextoAdicionalVoucher = ObtenerSubCampoISO63(Campo, "0029")
                objTrnResp.FirmarVoucher = (ObtenerSubCampoISO63(Campo, "0032") <> "0")  'Si esta "0"
                Texto = Trim(ObtenerSubCampoISO63(Campo, "0033"))
                If Texto <> "" Then
                    objTrnResp.TextoAdicionalVoucher = Texto
                End If
            End If
        End If

        Return objTrnResp
    End Function

End Class

