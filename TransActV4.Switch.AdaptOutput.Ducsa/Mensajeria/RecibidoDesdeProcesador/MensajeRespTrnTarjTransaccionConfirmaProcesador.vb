﻿Imports TransActV4.Common.ISO8583
Imports TransActV4.Switch.Entidad.Transacciones

Friend Class MensajeRespTrnTarjTransaccionConfirmaProcesador
    Inherits absMensajeRecibidoDelProcesador


    Private _objISO As ISO8583Mensaje

    Public Sub New(objISO As ISO8583Mensaje)
        _objISO = objISO
    End Sub

    Public Overrides Function ObtenerTransaccion() As TransaccionRespuesta
        Dim objTrnResp As TrnRespTarjTransaccionConfirmaProcesador = TransActV4.Switch.Entidad.Transacciones.Factory.GetInstance.ConstruirTrnRespTarjTransaccionConfirmaProcesador
        Dim FtoASCII As New TransActV4.Common.Formatter.FormatoASCII
        Dim Campo As String

        objTrnResp.Aprobada = False
        objTrnResp.Trace = Val(_objISO.ValorCampo("011"))
        objTrnResp.NroAutorizacion = _objISO.ValorCampo("038")
        objTrnResp.CodRespAdq = _objISO.ValorCampo("039")
        objTrnResp.TID = _objISO.ValorCampo("041")

        Campo = _objISO.ValorCampo("063")
        If Trim(Campo) <> "" Then
            If EsCampoISO63Texto(Campo) Then
                objTrnResp.TextoAdicionalPantalla = Mid(Campo, 1, 100)
            Else
                objTrnResp.TextoAdicionalPantalla = ObtenerSubCampoISO63(Campo, "0022")
            End If
        End If

        Return objTrnResp
    End Function
End Class
