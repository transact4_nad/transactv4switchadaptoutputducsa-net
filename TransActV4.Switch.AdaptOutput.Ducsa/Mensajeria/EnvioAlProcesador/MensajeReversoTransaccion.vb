﻿Imports TransActV4.Common.ISO8583
Imports TransActV4.Switch.Entidad.Transacciones
Imports TransActV4.Switch.Entidad.Switch
Friend Class MensajeReversoTransaccion
    Inherits absMensajeEnvioAProcesador



#Region "Declaraciones"
    Private Const TIPO_MENSAJE_COMPRA_REVERSO As String = "0400"

    Private _objTrnTarjReversoTransaccion As TrnTarjReversoTransaccion
    Private _objTrnTarjTransaccion As TrnTarjTransaccion


#End Region

    Public Sub New(objTrnTarjReversoTransaccion As TrnTarjReversoTransaccion)
        _objTrnTarjReversoTransaccion = objTrnTarjReversoTransaccion
        _objTrnTarjTransaccion = objTrnTarjReversoTransaccion.TransaccionAReversar
    End Sub

    Protected Overrides Function ConstruirISO() As ISO8583Mensaje
        Dim objISO As New ISO8583Mensaje(MENSAJE_CERRADO)
        objISO.LenHead = LEN_HEAD
        objISO.TPDU = _objTrnTarjReversoTransaccion.ProcTPDU
        objISO.TipoMensaje = TIPO_MENSAJE_COMPRA_REVERSO
        Return objISO
    End Function

    Protected Overrides Function ObtenerISO(MensajeISO As ISO8583Mensaje) As String
        Return MensajeISO.ToMensajeISO
    End Function

    Protected Overrides Sub CargarCampo001(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo002(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("002", _objTrnTarjTransaccion.PAN.PAN)
    End Sub

    Protected Overrides Sub CargarCampo003(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("003", "003000")
    End Sub

    Protected Overrides Sub CargarCampo004(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("004", Format(_objTrnTarjTransaccion.Monto, "000000000000"))
    End Sub

    Protected Overrides Sub CargarCampo005(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo006(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo007(MensajeISO As ISO8583Mensaje)
        Dim Campo As String
        If Date.Now < _objTrnTarjTransaccion.TmstRecibida Then
            Campo = Format(_objTrnTarjTransaccion.TmstRecibida, "MMdd")
        Else
            Campo = Format(Date.Now, "MMdd")
        End If
        If Format(Date.Now, "HHmmss") < Format(_objTrnTarjTransaccion.TmstRecibida, "HHmmss") Then
            Campo &= Format(_objTrnTarjTransaccion.TmstRecibida, "HHmmss")
        Else
            Campo &= Format(Date.Now, "HHmmss")
        End If
        MensajeISO.Campo("007", Campo)
    End Sub

    Protected Overrides Sub CargarCampo008(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo009(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo010(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo011(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("011", Right(Format(_objTrnTarjTransaccion.Trace, "000000"), 6))
    End Sub

    Protected Overrides Sub CargarCampo012(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("012", Format(_objTrnTarjTransaccion.TmstRecibida, "HHmmss"))
    End Sub

    Protected Overrides Sub CargarCampo013(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("013", Format(_objTrnTarjTransaccion.TmstRecibida, "MMdd"))
    End Sub

    Protected Overrides Sub CargarCampo014(MensajeISO As ISO8583Mensaje)
        Dim Vencimiento As String
        Vencimiento = Mid(_objTrnTarjTransaccion.Tarjeta.Vencimiento, 3, 2) & Mid(_objTrnTarjTransaccion.Tarjeta.Vencimiento, 1, 2)
        MensajeISO.Campo("014", Vencimiento)
    End Sub

    Protected Overrides Sub CargarCampo015(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo016(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo017(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo018(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo019(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo020(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo021(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo022(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("022", "012")
    End Sub

    Protected Overrides Sub CargarCampo023(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo024(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("024", ObtenerNIIByTPDU(_objTrnTarjTransaccion.ProcTPDU))
    End Sub

    Protected Overrides Sub CargarCampo025(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo026(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo027(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo028(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo029(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo030(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo031(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo032(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo033(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo034(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo035(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo036(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo037(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo038(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo039(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo040(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo041(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("041", _objTrnTarjTransaccion.Terminal.TID)
    End Sub

    Protected Overrides Sub CargarCampo042(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("042", _objTrnTarjTransaccion.Terminal.MID)
    End Sub

    Protected Overrides Sub CargarCampo043(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo044(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo045(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo046(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo047(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo048(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo049(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("049", IIf(_objTrnTarjTransaccion.Moneda.MonISO.Length > 3, Right(_objTrnTarjTransaccion.Moneda.MonISO, 3), _objTrnTarjTransaccion.Moneda.MonISO))
    End Sub

    Protected Overrides Sub CargarCampo050(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo051(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo052(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo053(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo054(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo055(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo056(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo057(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo058(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo059(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo060(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("060", VERSIONSOFT)
    End Sub

    Protected Overrides Sub CargarCampo061(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo062(MensajeISO As ISO8583Mensaje)
        Dim Campo As String
        Campo = Right(Format(_objTrnTarjTransaccion.Ticket, "000000"), 6)
        MensajeISO.Campo("062", Campo)
    End Sub

    Protected Overrides Sub CargarCampo063(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo064(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

End Class
