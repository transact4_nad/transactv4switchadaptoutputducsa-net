﻿Imports TransActV4.Common.ISO8583
Imports TransActV4.Switch.Entidad.Transacciones
Imports TransActV4.Switch.Entidad.Switch


Friend MustInherit Class absMensajeEnvioAProcesador

#Region "Declaraciones"
    Friend Const MENSAJE_CERRADO As Boolean = True
    Friend Const LEN_HEAD As Integer = 4
    Friend Const VERSIONSOFT As String = "50NEWAGET4 141"
    Friend Const ACQUIRING_COUNTRY_CODE As String = "0858"
#End Region


#Region "Abstract Factory de sus Hijos"

    Shared Function ConstruirMensajeEnviar(objTrn As Transaccion) As absMensajeEnvioAProcesador
        Select Case objTrn.TipoTrn
            Case Transaccion.TiposTransaccion.TIPOTRN_TARJETA_TRANSACCION
                Dim objTarj As TrnTarjTransaccion = DirectCast(objTrn, TrnTarjTransaccion)
                Return New MensajeTransaccion(objTrn)
            Case Transaccion.TiposTransaccion.TIPOTRN_TARJETA_REVERSO_TRANSACCION
                Return New MensajeReversoTransaccion(objTrn)
            Case Transaccion.TiposTransaccion.TIPOTRN_TARJETA_CIERRE
                Return New MensajeCierre(objTrn)
            Case Transaccion.TiposTransaccion.TIPOTRN_TARJETA_ECHOTEST
                Return New MensajeEchoTest(objTrn)
            Case Transaccion.TiposTransaccion.TIPOTRN_CONSULTA_SALDO
                Return New MensajeConsultaSaldo(objTrn)
            Case Transaccion.TiposTransaccion.TIPOTRN_TARJETA_TRANSACCION_CONFIRMA_PROCESADOR
                Return New MensajeTrnTarjTransaccionConfirmaProcesador(objTrn)
            Case Else
                Throw New ExceptionOutputAdapterTipoNoSoportado("TIPO TRANSACCION NO SOPORTADA POR OUTPUTADAPTER DUCSA, TIPO : " & objTrn.TipoTrn)
        End Select
    End Function

    Shared Function ObtenerProcId(objTrn As Transaccion) As Integer
        Dim ProcId As Integer = 0
        Select Case objTrn.TipoTrn
            Case Transaccion.TiposTransaccion.TIPOTRN_TARJETA_TRANSACCION
                Dim objTarj As TrnTarjTransaccion = DirectCast(objTrn, TrnTarjTransaccion)
                ProcId = objTarj.ProcId
                Return ProcId
            Case Transaccion.TiposTransaccion.TIPOTRN_TARJETA_REVERSO_TRANSACCION
                Dim objTarj As TrnTarjReversoTransaccion = DirectCast(objTrn, TrnTarjReversoTransaccion)
                ProcId = objTarj.TransaccionAReversar.ProcId
                Return ProcId
            Case Transaccion.TiposTransaccion.TIPOTRN_TARJETA_CIERRE
                Dim objTarj As TrnTarjCierre = DirectCast(objTrn, TrnTarjCierre)
                ProcId = objTarj.ProcId
                Return ProcId
            Case Transaccion.TiposTransaccion.TIPOTRN_TARJETA_ECHOTEST
                Return ProcId
            Case Transaccion.TiposTransaccion.TIPOTRN_CONSULTA_SALDO
                Dim objTarj As TrnTarjTransaccion = DirectCast(objTrn, TrnTarjTransaccion)
                ProcId = objTarj.ProcId
                Return ProcId
            Case Transaccion.TiposTransaccion.TIPOTRN_TARJETA_TRANSACCION_CONFIRMA_PROCESADOR
                Dim objTarj As TrnTarjTransaccionConfirmaProcesador = DirectCast(objTrn, TrnTarjTransaccionConfirmaProcesador)
                ProcId = objTarj.ProcId
                Return ProcId
            Case Else
                Throw New ExceptionOutputAdapterTipoNoSoportado("TIPO TRANSACCION NO SOPORTADA POR OUTPUTADAPTER DUCSA, TIPO : " & objTrn.TipoTrn)
        End Select
    End Function
#End Region

#Region "Metodos de sus Hijos"
    Friend Function ObtenerMedioISOByMedioTrn(Medio As String) As String
        Select Case Medio
            Case TrnTarjTransaccion.Medios.Banda
                Return "2"
            Case TrnTarjTransaccion.Medios.Manual
                Return "1"
            Case TrnTarjTransaccion.Medios.ChipEmv
                Return "5"
            Case TrnTarjTransaccion.Medios.ChipNfc
                Return "8"
            Case Else
                Throw New ExceptionOutputAdapterTipoNoSoportado("MEDIO NO SOPORTANDO POR OUTPUTADAPTER DUCSA | MEDIO=" & Medio)
        End Select
    End Function

    Friend Function ObtenerTipoTarjetaISOByMedioTrn(TipoTarjeta As String) As String
        Select Case TipoTarjeta
            Case AdqTarjProducto.TiposTarjeta.Debito
                Return "2"
            Case Else
                Return "1"
        End Select
    End Function

    Friend Function ObtenerNIIByTPDU(TPDU As String) As String
        Return Mid(TPDU, 4, 3)
    End Function

    Friend Function FormatoNroAutorizacion(NroAutorizacion As String) As String
        If Trim(NroAutorizacion) = "" Then NroAutorizacion = " "
        Return Mid(Replace(NroAutorizacion, " ", "").PadLeft(6, "0"), 1, 6)
    End Function
#End Region

#Region "Template Method"

    Public Function ObtenerMensajeProcesador() As String
        Dim objISO = ConstruirISO()
        CargarCampo001(objISO)
        CargarCampo002(objISO)
        CargarCampo003(objISO)
        CargarCampo004(objISO)
        CargarCampo005(objISO)
        CargarCampo006(objISO)
        CargarCampo007(objISO)
        CargarCampo008(objISO)
        CargarCampo009(objISO)
        CargarCampo010(objISO)
        CargarCampo011(objISO)
        CargarCampo012(objISO)
        CargarCampo013(objISO)
        CargarCampo014(objISO)
        CargarCampo015(objISO)
        CargarCampo016(objISO)
        CargarCampo017(objISO)
        CargarCampo018(objISO)
        CargarCampo019(objISO)
        CargarCampo020(objISO)
        CargarCampo021(objISO)
        CargarCampo022(objISO)
        CargarCampo023(objISO)
        CargarCampo024(objISO)
        CargarCampo025(objISO)
        CargarCampo026(objISO)
        CargarCampo027(objISO)
        CargarCampo028(objISO)
        CargarCampo029(objISO)
        CargarCampo030(objISO)
        CargarCampo031(objISO)
        CargarCampo032(objISO)
        CargarCampo033(objISO)
        CargarCampo034(objISO)
        CargarCampo035(objISO)
        CargarCampo036(objISO)
        CargarCampo037(objISO)
        CargarCampo038(objISO)
        CargarCampo039(objISO)
        CargarCampo040(objISO)
        CargarCampo041(objISO)
        CargarCampo042(objISO)
        CargarCampo043(objISO)
        CargarCampo044(objISO)
        CargarCampo045(objISO)
        CargarCampo046(objISO)
        CargarCampo047(objISO)
        CargarCampo048(objISO)
        CargarCampo049(objISO)
        CargarCampo050(objISO)
        CargarCampo051(objISO)
        CargarCampo052(objISO)
        CargarCampo053(objISO)
        CargarCampo054(objISO)
        CargarCampo055(objISO)
        CargarCampo056(objISO)
        CargarCampo057(objISO)
        CargarCampo058(objISO)
        CargarCampo059(objISO)
        CargarCampo060(objISO)
        CargarCampo061(objISO)
        CargarCampo062(objISO)
        CargarCampo063(objISO)
        CargarCampo064(objISO)
        Return ObtenerISO(objISO)
    End Function

    Protected MustOverride Function ConstruirISO() As ISO8583Mensaje
    Protected MustOverride Function ObtenerISO(MensajeISO As ISO8583Mensaje) As String
    Protected MustOverride Sub CargarCampo001(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo002(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo003(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo004(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo005(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo006(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo007(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo008(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo009(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo010(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo011(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo012(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo013(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo014(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo015(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo016(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo017(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo018(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo019(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo020(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo021(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo022(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo023(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo024(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo025(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo026(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo027(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo028(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo029(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo030(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo031(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo032(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo033(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo034(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo035(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo036(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo037(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo038(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo039(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo040(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo041(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo042(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo043(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo044(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo045(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo046(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo047(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo048(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo049(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo050(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo051(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo052(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo053(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo054(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo055(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo056(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo057(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo058(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo059(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo060(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo061(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo062(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo063(MensajeISO As ISO8583Mensaje)
    Protected MustOverride Sub CargarCampo064(MensajeISO As ISO8583Mensaje)

#End Region

End Class
