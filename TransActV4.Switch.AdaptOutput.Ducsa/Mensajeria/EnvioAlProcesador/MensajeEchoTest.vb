﻿Imports TransActV4.Common.ISO8583
Imports TransActV4.Switch.Entidad.Transacciones
Friend Class MensajeEchoTest
    Inherits absMensajeEnvioAProcesador

#Region "Declaraciones"
    Private Const TIPO_MENSAJE_ECHOTEST As String = "0800"
    Private Const TID_ECHOTEST As String = "F9999199"
    Private Const MID_ECHOTEST As String = "123456789"

    Private _objTrnTarjEchoTest As TrnTarjEchoTest
#End Region

    Public Sub New(objTrnTarjEchoTest As TrnTarjEchoTest)
        _objTrnTarjEchoTest = objTrnTarjEchoTest
    End Sub

    Protected Overrides Function ConstruirISO() As ISO8583Mensaje
        Dim objISO As New ISO8583Mensaje(MENSAJE_CERRADO)
        objISO.LenHead = LEN_HEAD
        objISO.TPDU = _objTrnTarjEchoTest.ProcTPDU
        objISO.TipoMensaje = TIPO_MENSAJE_ECHOTEST
        Return objISO
    End Function

    Protected Overrides Function ObtenerISO(MensajeISO As ISO8583Mensaje) As String
        Return MensajeISO.ToMensajeISO
    End Function

    Protected Overrides Sub CargarCampo001(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo002(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo003(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("003", "990000")
    End Sub

    Protected Overrides Sub CargarCampo004(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo005(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo006(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo007(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("007", Format(_objTrnTarjEchoTest.TmstRecibida, "MMdd") & Format(_objTrnTarjEchoTest.TmstRecibida, "HHmmss"))
    End Sub

    Protected Overrides Sub CargarCampo008(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo009(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo010(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo011(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("011", Right(Format(_objTrnTarjEchoTest.Trace, "000000"), 6))
    End Sub

    Protected Overrides Sub CargarCampo012(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo013(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo014(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo015(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo016(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo017(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo018(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo019(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo020(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo021(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo022(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo023(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo024(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("024", ObtenerNIIByTPDU(_objTrnTarjEchoTest.ProcTPDU))
    End Sub

    Protected Overrides Sub CargarCampo025(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo026(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo027(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo028(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo029(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo030(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo031(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo032(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo033(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo034(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo035(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo036(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo037(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo038(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo039(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo040(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo041(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("041", TID_ECHOTEST)
    End Sub

    Protected Overrides Sub CargarCampo042(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("042", MID_ECHOTEST)
    End Sub

    Protected Overrides Sub CargarCampo043(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo044(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo045(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo046(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo047(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo048(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo049(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo050(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo051(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo052(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo053(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo054(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo055(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo056(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo057(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo058(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo059(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo060(MensajeISO As ISO8583Mensaje)
        MensajeISO.Campo("060", VERSIONSOFT)
    End Sub

    Protected Overrides Sub CargarCampo061(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo062(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo063(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub

    Protected Overrides Sub CargarCampo064(MensajeISO As ISO8583Mensaje)
        'VACIO
    End Sub


End Class
