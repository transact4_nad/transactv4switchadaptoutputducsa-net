﻿Imports TransActV4.Switch.Interface.INegocio
Imports TransActV4.Switch.Interface.IAdaptOutput
Imports TransActV4.Switch.Entidad.Transacciones
Public Class AdaptOutput_OUT
    Implements IAdaptOutput_OUT

    Private _objNegocio As INegocio
    Private _TrazaEncendida As Boolean
    Private Const NOMBRE_CONCENTRADOR As String = "DUCSA"
    Private Const CARPETA_TRAZA As String = "Traza\"

    Public Sub Inicializar(objNeg As INegocio, TrazaEncendida As Boolean) Implements IAdaptOutput_OUT.Inicializar
        _objNegocio = objNeg
        _TrazaEncendida = TrazaEncendida
    End Sub

    Public Function ObtenerMensaje(objTrn As Transaccion) As String Implements IAdaptOutput_OUT.ObtenerMensaje
        Dim strMensaje As String = ""
        Dim ProcId As Integer = 0
        Dim Mensaje As absMensajeEnvioAProcesador
        Mensaje = absMensajeEnvioAProcesador.ConstruirMensajeEnviar(objTrn)
        ProcId = absMensajeEnvioAProcesador.ObtenerProcId(objTrn)

        strMensaje = Format(ProcId, "00") & Mensaje.ObtenerMensajeProcesador
        strMensaje = Format(Len(strMensaje), "0000") & strMensaje

        GrabarDebug(strMensaje, objTrn)
        Return strMensaje
    End Function

#Region "Debug"
    Private Sub GrabarDebug(strMensaje As String, objTrn As Transaccion)
        If Not _TrazaEncendida Then Exit Sub
        If objTrn.TipoTrn = Transaccion.TiposTransaccion.TIPOTRN_TARJETA_ECHOTEST Then Exit Sub
        Dim SW As System.IO.StreamWriter = Nothing
        Dim FtoASCII As New TransActV4.Common.Formatter.FormatoASCII
        Try
            If Not IO.Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory & CARPETA_TRAZA) Then
                IO.Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory & CARPETA_TRAZA)
            End If
            Dim RutaArchivo As String = System.AppDomain.CurrentDomain.BaseDirectory & CARPETA_TRAZA & NOMBRE_CONCENTRADOR & "_" & Format(Val(Mid(objTrn.TrnId, 1, 8)), "00000000") & "_" & Now.Ticks & ".txt"
            SW = New System.IO.StreamWriter(RutaArchivo)
            SW.Write(FtoASCII.ASCIIToHEX(strMensaje))
        Catch ex As Exception
        Finally
            If Not IsNothing(SW) Then
                SW.Close()
            End If
        End Try
    End Sub
#End Region

End Class
